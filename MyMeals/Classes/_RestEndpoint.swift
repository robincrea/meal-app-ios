//
//  RestEndpoint.swift
//  Meals
//
//  Created by Henri LA on 03.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

struct RestEndpoint {
  
  private let prefixPath = "/api/json"
  
  let scheme: String
  let host: String
  let version: String
  let apiKey: String
  
  public init(scheme: String, host: String, version: String, apiKey: String){
    self.scheme = scheme
    self.host = host
    self.version = version
    self.apiKey = apiKey
  }
  
  public var url: URL? {
    var urlComponents = URLComponents()
    
    urlComponents.scheme = scheme
    urlComponents.host = host
    
    return urlComponents.url
  }
  
  func url(forPath path: String, andQueryItems queryItems: [URLQueryItem]?) -> URL? {
    var urlComponents = URLComponents()
    
    urlComponents.scheme = scheme
    urlComponents.host = host
    urlComponents.path = prefixPath + "/\(version)/\(apiKey)" + path
    urlComponents.queryItems = queryItems
    
    return urlComponents.url
  }
  
}
