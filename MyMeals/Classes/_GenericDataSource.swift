//
//  GenericDataSource.swift
//  MyMeals
//
//  Created by Henri LA on 09.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

protocol GenericDataSource {
  associatedtype T
  
  func reload()
  
  func numberOfSections() -> Int
  func numberOfItems(in section: Int) -> Int
  func item(at indexPath: IndexPath) -> T?
}

protocol GenericDataSourceReloadable: AnyObject {
  func dataSourceDidReload(with error: Error?)
}

protocol TableViewQuickSearch {
  func sectionIndexTitles() -> [String]?
  func titleForSection(_ section: Int) -> String?
}

typealias TableViewDataSourceQuickSearch = GenericDataSource & TableViewQuickSearch
