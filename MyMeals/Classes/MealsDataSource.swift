//
//  MealsDataSource.swift
//  MyMeals
//
//  Created by Henri LA on 09.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

class MealsDataSource: GenericDataSource {
    
    
  typealias T = Meal
  
  private let mealCategory: MealCategory
  private let mealRestAPI: MealRestAPI
  
  weak var delegate: GenericDataSourceReloadable?
  
  private var dataSource = [Meal]()
  
  init(_ mealCategory: MealCategory, mealRestAPI: MealRestAPI, delegate: GenericDataSourceReloadable){
    self.mealCategory = mealCategory
    self.mealRestAPI = mealRestAPI
    self.delegate = delegate
  }
  
  // MARK: GenericDataSource implementation
  
  func reload() {
    mealRestAPI.fetchMealsForCategoryName(self.mealCategory.strCategory) {  (arg) in
        let (mealItems, error) = arg
        self.formatMeals(meals: mealItems)
        self.delegate?.dataSourceDidReload(with: error)
    }
    // TODO: To complete
  }

  func numberOfSections() -> Int {
    return 1
  }
  
  func numberOfItems(in section: Int) -> Int {
    return dataSource.count
  }
  
  func item(at indexPath: IndexPath) -> Meal? {
    return dataSource[indexPath.row]
  }
    
    private func formatMeals(meals mealsItems: [Meal]){
        for meal in mealsItems{
            dataSource.append(meal)
        }
    }

  
}
