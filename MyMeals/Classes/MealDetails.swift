//
//  Meal.swift
//  Meals
//
//  Created by Henri LA on 05.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

struct MealDetails: Codable {
  
  let idMeal: String
  let strMeal: String?
  let strDrinkAlternate: String?
  let strCategory: String?
  let strArea: String?
  let strInstructions: String?
  let strMealThumb: String?
  let strTags: String?
  let strYoutube: String?
  let strSource: String?
  let dateModified: String?
  
  init(_ dictionary: [String: Any]) {
    idMeal = dictionary["idMeal"] as? String ?? ""
    strMeal = dictionary["strMeal"] as? String
    strDrinkAlternate = dictionary["strDrinkAlternate"] as? String
    strCategory = dictionary["strCategory"] as? String
    strArea = dictionary["strArea"] as? String
    strInstructions = dictionary["strInstructions"] as? String
    strTags = dictionary["strTags"] as? String
    strMealThumb = dictionary["strMealThumb"] as? String
    strYoutube = dictionary["strYoutube"] as? String
    strSource = dictionary["strSource"] as? String
    dateModified = dictionary["dateModified"] as? String
  }
  
  var mealTags: [String] {
    #warning("To complete the implementation")
    // TODO: Complete by 'splitting' strTags into an array of String and return this array !
    var tags = [String]()
    if(strTags != nil){
        tags = strTags!.components(separatedBy: ",") // return an [String]
    }
   
    return tags
  }
  
  var imageURL: URL? {
    #warning("To complete the implementation - strMealThumb is the stringURL for the image!")
   
      let result: URL?
        if let strMealThumbURL = strMealThumb {
          result = URL(string: strMealThumbURL)
        } else {
          result = nil
        }
        
        return result
      }
}

extension MealDetails {
  
  static func meal(from responseResult: ResponseResult) -> MealDetailsResponse {
    var result: MealDetailsResponse
    result.mealDetails = nil
    result.error = nil
    
    #warning("To complete the implementation based on MealCategory example from the func: mealCategories")
    // TODO: Set error
    
    // TODO: Set result.mealDetails -> it expects a MealDetails
    // Hint: 1.Get the data 2. Create a JSON (Dictionary/Array) and create MealDetails and assign it to
    //       result.mealDetails = ...
    // Set the error from responseResult to result.1
      result.1 = responseResult.error
      
      if let data = responseResult.data,
        let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
        let meals = json["meals"] as? [[String: Any]]
      {
        var dataResult = [MealDetails]()
        
        meals.forEach{ dataResult.append(MealDetails($0)) }
        
        result.mealDetails = dataResult[0]
      }
      
      return result

  }
  
  static func meals(from responseResult: ResponseResult) -> SearchedMealsDetailsResponse {
    var result: SearchedMealsDetailsResponse
    result.arrayMealDetails = []
    result.error = nil
    
    // TODO: Set error
    result.1 = responseResult.error
         
         if let data = responseResult.data,
           let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
           let meals = json["meals"] as? [[String: Any]]
         {
           var dataResult = [MealDetails]()
           
           meals.forEach{ dataResult.append(MealDetails($0)) }
           
           result.arrayMealDetails = dataResult
         }
         
         return result


  }
  
}
