//
//  NoIndexMealCategoriesDataSource.swift
//  MyMeals
//
//  Created by Henri LA on 11.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

class NoIndexMealCategoriesDataSource: TableViewDataSourceQuickSearch {
  typealias T = MealCategory
  
  // MARK: Variables configuration
  
  private let mealRestAPI: MealRestAPI
  private var reloadDelegate: GenericDataSourceReloadable?
  
  // MARK: Private Variables
  
  private var dataSource = [MealCategory]()
  
  // MARK: Inits
  
  init(mealRestAPI: MealRestAPI, reloadDelegate: GenericDataSourceReloadable){
    self.mealRestAPI = mealRestAPI
    self.reloadDelegate = reloadDelegate
  }
  
  // MARK: TableViewDataSourceQuickSearch implementation
  
  func reload() {
    mealRestAPI.fetchMealCategories { [weak self] (mealCategories, error) in
      self?.dataSource = mealCategories
      self?.reloadDelegate?.dataSourceDidReload(with: error)
    }
  }
  
  func numberOfSections() -> Int {
    return 1
  }
  
  func numberOfItems(in section: Int) -> Int {
    return dataSource.count
  }
  
  func item(at indexPath: IndexPath) -> MealCategory? {
    return dataSource[indexPath.row]
  }
  
  func sectionIndexTitles() -> [String]? {
    return nil
  }
  
  func titleForSection(_ section: Int) -> String? {
    return nil
  }
  
}
