//
//  MealCategory.swift
//  Meals
//
//  Created by Henri LA on 03.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

struct MealCategory: Codable {
  
  let idCategory: String?
  let strCategory: String
  let strCategoryThumb: String?
  let strCategoryDescription: String?
  
  init(dictionary: [String: String] ){
    idCategory = dictionary["idCategory"]
    strCategory = dictionary["strCategory"] ?? ""
    strCategoryThumb = dictionary["strCategoryThumb"]
    strCategoryDescription = dictionary["strCategoryDescription"]
  }
  
  var imageURL: URL? {
    let result: URL?
    if let thumbStringURL = strCategoryThumb {
      result = URL(string: thumbStringURL)
    } else {
      result = nil
    }
    
    return result
  }
  
}

extension MealCategory {
  
  static func mealCategories(from responseResult: ResponseResult) -> MealCategoriesResponse {
    var result: MealCategoriesResponse
    result.mealCategories = []
    result.error = nil
    
    // Set the error from responseResult to result.1
    result.1 = responseResult.error
    
    // 1. Unwrap the responseResult.data as it is optional
    // 2. Serialize the data into a dictionary [String: Any] because of the format received
    // 3. Get the array of dictionary from the key 'categories' based on the JSON structured
    if let data = responseResult.data,
      let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
      let categories = json["categories"] as? [[String: String]]
    {
      var dataResult = [MealCategory]()
      
      categories.forEach{ dataResult.append(MealCategory(dictionary: $0)) }
      
      result.mealCategories = dataResult
    }
    
    return result
  }
  
}
