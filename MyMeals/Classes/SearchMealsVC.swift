//
//  SearchMealsVC.swift
//  MyMeals
//
//  Created by Henri LA on 10.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class SearchMealsVC: UITableViewController {
  
  private lazy var mealRestAPI = UIApplication.appDelegate.mealRestAPI
  private var lastSessionDataTask: URLSessionDataTask?
  private var result = [MealDetails]()
  
  private lazy var searchController: UISearchController = {
    let controller = UISearchController(searchResultsController: nil)
    controller.searchResultsUpdater = self
    controller.obscuresBackgroundDuringPresentation = false
    controller.searchBar.placeholder = NSLocalizedString("Search meal...", comment: "")
    
    return controller
  }()
  
  // MARK: Init
  
  init() {
    super.init(nibName: nil, bundle: nil)

    title = NSLocalizedString("_search_mealvc_title_", comment: "Search Title")
    
    tabBarItem = UITabBarItem(title: title, image: UIImage(named: "tab2"), tag: 1)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationItem.searchController = searchController
    
    tableView.register(UINib(nibName: "GenericMealTableViewCell", bundle: nil),
                       forCellReuseIdentifier: "GenericMealTableViewCell")
  }
  
  @objc private func dismissKeyboard(){
    UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder),
                                    to: nil,
                                    from: nil,
                                    for: nil)
  }
  
  // MARK:
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return result.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "GenericMealTableViewCell") as? GenericMealTableViewCell else {
      return UITableViewCell()
    }
    
    cell.configCellForModel(result[indexPath.row], with: UIApplication.appDelegate.imageStore)
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    
    mealRestAPI.fetchMealDetailsForMealID(result[indexPath.row].idMeal) { (mealDetailsResponse) in
           self.handleMealDetailsResponse(mealDetailsResponse, at:indexPath)
       }

  }
  
  private func handleMealDetailsResponse(_ mealDetailsResponse: MealDetailsResponse, at indexPath: IndexPath) {
    // TODO: Check in mealDetailsResponse, if there is an error. If there is, display ONLY UIAlertViewController
  
    if(mealDetailsResponse.error != nil || mealDetailsResponse.mealDetails == nil){
        let alertController = UIAlertController(title: NSLocalizedString("_alertViewController_title_", comment: "Error Title"), message: "Error on fetching meal details", preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {
                action in
                 // Called when user taps outside
            }))
        self.present(alertController, animated: true, completion: nil)
    }

    // TODO: If there is no error, check if mealDetailsResponse has a MealDetails.
    if(mealDetailsResponse.mealDetails != nil){
        let modal = ModalFullScreenNavigationVC(rootViewController: MealDetailsVC(mealDetails: mealDetailsResponse.mealDetails!))
        DispatchQueue.main.async { [weak self] in
            self?.present(
                modal,
                animated: true
            )
        }
    }
    
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

extension SearchMealsVC: UISearchResultsUpdating {
  
  func updateSearchResults(for searchController: UISearchController) {
    lastSessionDataTask?.cancel()
    guard let text = searchController.searchBar.text, !text.isEmpty else {
      handleResult(([], nil))
      return
    }
    lastSessionDataTask = mealRestAPI.fetchArrayOfMealDetailsForMealName(text) { [weak self] (response) in
      self?.handleResult(response)
    }
  }
  
  private func handleResult(_ response: SearchedMealsDetailsResponse){
    result = response.arrayMealDetails
    print(result)
    DispatchQueue.main.async {
      self.tableView.reloadData()
    }
  }
  
}
