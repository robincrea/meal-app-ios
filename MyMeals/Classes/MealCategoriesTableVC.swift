//
//  MealCategoriesTableVC.swift
//  MyMeals
//
//  Created by Henri LA on 09.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class MealCategoriesTableVC: UITableViewController {
  
  // MARK: Variables configuration
  
 
  private lazy var mealCategoriesDataSource = MealCategoriesDataSource(mealRestAPI: UIApplication.appDelegate.mealRestAPI, reloadDelegate: self)
  private let reuseIdentifier = GenericMealTableViewCell.reuseIdentifier;
  // MARK: Init
  
  init() {
    super.init(nibName: nil, bundle: nil)
    
    title = NSLocalizedString("_meal_categories_tablevc_title_", comment: "Discover Title")
    
    tabBarItem = UITabBarItem(title: title, image: UIImage(named: "tab1"), tag: 0)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    mealCategoriesDataSource.reload()
    
     
 
     let nibMealCell = UINib(nibName: reuseIdentifier, bundle: Bundle.main)
     tableView.register(nibMealCell, forCellReuseIdentifier: reuseIdentifier)

  }
  
  // MARK: TableView data source & delegate
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return mealCategoriesDataSource.numberOfSections()
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // TODO: Complete the missing implementation
    return mealCategoriesDataSource.numberOfItems(in: section)
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

    if let genericMealCell = cell as? GenericMealTableViewCell,
        let genericMealData = mealCategoriesDataSource.item(at: indexPath){
   
        genericMealCell.configCellForModel(genericMealData, with: UIApplication.appDelegate.imageStore)
    }

    return cell
  }
  
  override func sectionIndexTitles(for tableView: UITableView) -> [String]? {

    return mealCategoriesDataSource.sectionIndexTitles()
  }
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return mealCategoriesDataSource.titleForSection(section);
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    let category = mealCategoriesDataSource.item(at: indexPath)
    let mealVC = MealsTableVC(mealCategory: category!);
    navigationController?.pushViewController(mealVC, animated: true)
    tableView.deselectRow(at: indexPath, animated: true)

  }
  
}

// MARK: GenericDataSourceReloadable implementation for MealCategoriesTableVC

extension MealCategoriesTableVC: GenericDataSourceReloadable {
  
  func dataSourceDidReload(with error: Error?) {
    
   
    if(error != nil){
        let alertController = UIAlertController(title: NSLocalizedString("_alertViewController_title_", comment: "Error Title"), message: error?.localizedDescription, preferredStyle: .actionSheet)
          alertController.addAction(UIAlertAction(title:  NSLocalizedString("_alertViewController_ok_", comment: "Error OK"), style: .cancel, handler: {
              action in
                   // Called when user taps outside
          }))
          self.present(alertController, animated: true, completion: nil)
    }
   
    tableView.reloadData()
  }
  
}
