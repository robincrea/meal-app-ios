//
//  MealDetailsVC.swift
//  MyMeals
//
//  Created by Henri LA on 10.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class MealDetailsVC: UIViewController {

  // MARK: UIComponents

  @IBOutlet private weak var imageView: UIImageView!
  @IBOutlet private weak var categoryTitleLabel: UILabel!
  @IBOutlet private weak var categoryValueLabel: UILabel!
  @IBOutlet private weak var areaTitleLabel: UILabel!
  @IBOutlet private weak var areaValueLabel: UILabel!
  @IBOutlet private weak var lastTimeSeenTitleLabel: UILabel!
  @IBOutlet private weak var lastTimeSeenValueLabel: UILabel!
  @IBOutlet private weak var tagsCollectionView: UICollectionView!
  @IBOutlet private weak var instructionsLabel: UILabel!
  
  private let mealDetails: MealDetails
  private var imageIdentifier: String?

    let reuseIdentifier = "TagCell" // also enter this string as the cell identifier in the storyboard



let nameArr = ["Ram" , "Shyam", "Joe", "Mark", "Richard", "Remo"]

  // MARK: Inits
  
  init(mealDetails: MealDetails) {
    self.mealDetails = mealDetails
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // TODO: Set the title from mealDetails by using strMeal
    self.title = mealDetails.strMeal
    self.categoryTitleLabel.text = NSLocalizedString("_meal_category_label_", comment: "Category Title")
    self.areaTitleLabel.text = NSLocalizedString("_meal_area_label_", comment: "Area Title")
    self.categoryValueLabel.text = mealDetails.strCategory
    self.areaValueLabel.text = mealDetails.strArea
    
    // self.tagsCollectionView.register(TagCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    
    let nibTagCell = UINib(nibName: reuseIdentifier, bundle: Bundle.main)
    self.tagsCollectionView.register(nibTagCell, forCellWithReuseIdentifier: reuseIdentifier)
    
    self.tagsCollectionView.dataSource = self
    self.tagsCollectionView.delegate = self
    let image = UIImage(named: "closeButton")
    let closeBtn = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(closeVC))
 
    
    self.navigationItem.setLeftBarButton(closeBtn, animated: true)
    // TODO: Add an UIBarButtonItem to navigationItem.setLeftBarButton(...)
    // - By clicking on the UIBarButton, the ViewController will "dismiss"
    // - In order to make it works, you need to use the func closeVC()
    
    
    
    // TODO: Configure the ViewController by adding all data from MealDetails to your UIComponent !
    self.instructionsLabel.text = mealDetails.strInstructions
    self.configureImageForURL(mealDetails.imageURL!,  and: UIApplication.appDelegate.imageStore)
    

  }
  
  // MARK: Action(s)
  
  @objc private func closeVC(){
    // Dimiss the modal viewController
    dismiss(animated: true, completion: nil)
  }
    
    private func configureImageForURL(_ imageURL: URL, and imageStore: ImageStore) {
      // Set the imageIdentifier
      imageIdentifier = imageURL.absoluteString
      
      // Load the image from the url
      imageStore.loadImageFromURL(imageURL, for: imageURL.absoluteString) { [weak self] (image, identifier) in
        // Check if we have an image AND if the imageIdentifier is matching with identifier
        guard let image = image, self?.imageIdentifier == identifier else {
          return
        }
        
        // If it is fine, just assign it !
        DispatchQueue.main.async {
          self?.imageView?.image = image
        }
      }
    }

}

// MARK: Bonus!

extension MealDetailsVC: UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
     
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return mealDetails.mealTags.count
  }
     
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath) as! TagCell
     cell.titleLabel.text = mealDetails.mealTags[indexPath.row]
     return cell
}
}

extension MealDetailsVC: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

    return CGSize(width: 120, height: 68)
  }
  
}




