//
//  MealsTableVC.swift
//  MyMeals
//
//  Created by Henri LA on 10.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class MealsTableVC: UITableViewController {
    
  private let reuseIdentifier = GenericMealTableViewCell.reuseIdentifier;

  private lazy var dataSource = MealsDataSource(mealCategory, mealRestAPI: UIApplication.appDelegate.mealRestAPI, delegate: self)
    
  private let mealRestAPI: MealRestAPI
  
  // MARK: Variables configuration
  
  private let mealCategory: MealCategory
  
  // MARK: Init
  
  init(mealCategory: MealCategory) {
    self.mealCategory = mealCategory
    self.mealRestAPI =  UIApplication.appDelegate.mealRestAPI;
    super.init(nibName: nil, bundle: nil)
    title = mealCategory.strCategory
  }
  
  required init?(coder: NSCoder) {

    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()

    // TODO: Create a UIRefreshControl and assign it to 'refreshControl' of tableView
    self.refreshControl = UIRefreshControl()
    self.refreshControl?.tintColor = UIColor.purple

    self.refreshControl?.addTarget(self, action: #selector(reload), for: .valueChanged)
    // 1. Hint: refresControl = ...
    // 2. The UIRefreshControl tint color has to be purple !
    // 3. The UIRefreshControl need to addTarget -> self, #selector(reload), .valueChanged
    
    // TODO: Register the tableViewCell you created
    dataSource.reload()
    
     let nibMealCell = UINib(nibName: reuseIdentifier, bundle: Bundle.main)
     tableView.register(nibMealCell, forCellReuseIdentifier: reuseIdentifier)
    

  }
  
  @objc private func reload(){
    dataSource.reload()
    
    // Okok on avoue on est un peu des margoulins
    // On voulait faire un callback sur dataSource.reload() mais pas réussi
    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
       self.refreshControl?.endRefreshing();
    }

  }

  
  
  // MARK: Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
   
    return dataSource.numberOfSections()
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

    return dataSource.numberOfItems(in: section)
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

    if let genericMealCell = cell as? GenericMealTableViewCell,
        let genericMealData = dataSource.item(at: indexPath){
        
        genericMealCell.configCellForModel(genericMealData, with: UIApplication.appDelegate.imageStore)
    }
    

    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    mealRestAPI.fetchMealDetailsForMealID(dataSource.item(at: indexPath)!.idMeal) { (mealDetailsResponse) in
        self.handleMealDetailsResponse(mealDetailsResponse, at:indexPath)
    }

    
  }
  
  // MARK: Handle response
  
  private func handleMealDetailsResponse(_ mealDetailsResponse: MealDetailsResponse, at indexPath: IndexPath) {
    // TODO: Check in mealDetailsResponse, if there is an error. If there is, display ONLY UIAlertViewController
  
    if(mealDetailsResponse.error != nil || mealDetailsResponse.mealDetails == nil){
        let alertController = UIAlertController(title: NSLocalizedString("_alertViewController_title_", comment: "Error Title"), message: "Error on fetching meal details", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {
            action in
                 // Called when user taps outside
        }))
        self.present(alertController, animated: true, completion: nil)
    }

    // TODO: If there is no error, check if mealDetailsResponse has a MealDetails.
    if(mealDetailsResponse.mealDetails != nil){
        let modal = ModalFullScreenNavigationVC(rootViewController: MealDetailsVC(mealDetails: mealDetailsResponse.mealDetails!))
        DispatchQueue.main.async { [weak self] in
            self?.present(
                modal,
                animated: true
            )
        }
    }
    // TODO: Deselect the cell
    tableView.deselectRow(at: indexPath, animated: true)

  }
  
}

extension MealsTableVC: GenericDataSourceReloadable {
  
  func dataSourceDidReload(with error: Error?) {

    
    if(error != nil){
        let alertController = UIAlertController(title: NSLocalizedString("_alertViewController_title_", comment: "Error Title"), message: error?.localizedDescription, preferredStyle: .actionSheet)
          alertController.addAction(UIAlertAction(title:  NSLocalizedString("_alertViewController_ok_", comment: "Error OK"), style: .cancel, handler: {
              action in
                   // Called when user taps outside
          }))
          self.present(alertController, animated: true, completion: nil)
    }
    
    tableView.reloadData()
  }
  
}
